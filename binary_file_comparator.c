#include <stdio.h>
#include<time.h> 
int compare_two_binary_files(FILE *,FILE *);

FILE *fp1,*fp2;

int main(int argc, char *argv[])
{
    clock_t start, end;
    double cpu_time_used;
    int flag=0; 	
    unsigned char differByte[16];

	start=clock();	
/* Check for sufficient argumensts. check if file_1 and file_2 readable*/
    if (argc < 3)
    {
        printf("\nInsufficient Arguments: \n");
        printf("\nHelp:./executable <filename1> <filename2>\n");
        return -1;
    }
    else
    {
        fp1 = fopen(argv[1],  "r");
        if (fp1 == NULL)
        {
            printf("\nError in opening file %s", argv[1]);
            return -1;
        }
 
        fp2 = fopen(argv[2], "r");
 
        if (fp2 == NULL)
        {
            printf("\nError in opening file %s", argv[2]);
            return -1;
        }
 
        if ((fp1 != NULL) && (fp2 != NULL))
        {
            flag=compare_two_binary_files(fp1, fp2);
        }
    }

/* calculate execution time*/
        end=clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
 
        printf("Execution Time : %f milliseconds \n",cpu_time_used*1000);

/* check whether the two files are equal and printing 16 bytes from the point of difference*/

    if (flag == 0)
    {
        printf("Two files are not equal : the poistion at which two files differ is %ld:\n", ftell(fp1)+1);
        fread((unsigned char*)differByte,1,16,fp1);
	printf("\nFirst 16 bytes of first file from position it differ:");
        for(int i=0;i<16;i++)
                printf("\n %x",differByte[i] & 0xff);
	
	printf("\nFirst 16 bytes of Second file from position it differ:");
        fread((unsigned char*)differByte,1,16,fp2);
        for(int i=0;i<16;i++)
                printf("\n %x",differByte[i] & 0xff);
    }
    else
    {
        printf("Two files are Equal\n ");
    }
    
 return 0;
}
 
/*
 * compare two binary files character by character
 */
int compare_two_binary_files(FILE *fp1, FILE *fp2)
{
    char ch1, ch2;
    int flag = 0;
 

    while (((ch1 = fgetc(fp1)) != EOF) &&((ch2 = fgetc(fp2)) != EOF))
    {
        /*
          * character by character comparision
          * if equal then continue by comparing till the end of files
          */
        if (ch1 == ch2)
        {
            flag = 1;
            continue;
        }
        /*
          * If not equal then returns the byte position
          */
        else
        {
            fseek(fp1, -1, SEEK_CUR);        
            flag = 0;
            break;
        }
    }
	
return flag;
}

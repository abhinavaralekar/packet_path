# Packet_Path

Programming exercise (Take Home Part) -

File Comparison

Problem Statement - 
Write a C program to compare 2 binary files. It should take the 2 file names as command line parameters. (It has to be command line parameter and **not** accepting input from the user after the program is launched.)


File Compilation commands

gcc binary_file_comparator.c -o <executable_file>

Execution command

./<executable_file> <binary_file_1> <binary_file_2>
